﻿namespace EmailSender.Services.Emails.Domain.Entities
{
    public enum Status
    {
        Pending,
        Sent
    }
}
