﻿using EmailSender.Services.Emails.Domain.Entities;
using System;
using System.Collections.Generic;

namespace EmailSender
{
    public class Email
    {
        public long Id { get; set; }
        public Guid PublicId { get; set; }
        public EmailAddress From { get; set; }
        public ICollection<EmailEmailAddress> To { get; set; }
        public string Sender { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public Status Status { get; set; }
        public DateTime DateSent { get; set; }
        public string Priority { get; set; }

        public Email()
        {
            To = new HashSet<EmailEmailAddress>();
            PublicId = Guid.NewGuid();
            Status = Status.Pending;
        }

        public void SetSent()
        {
            DateSent = DateTime.Now;
            Status = Status.Sent;
        }

        public void SetBody(string body)
        {
            Body = body;
        }

        public void SetSubject(string subject)
        {
            Subject = subject;
        }

        public void SetSender(string sender)
        {
            Sender = sender;
        }

        public void SetFrom(EmailAddress from)
        {
            From = from;
        }

        public void SetTo(ICollection<EmailEmailAddress> to)
        {
            To = to;
        }
    }
}
