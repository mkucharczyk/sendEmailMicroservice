﻿using System;
using System.Collections.Generic;

namespace EmailSender.Services.Emails.Domain.Entities
{
    public class EmailAddress
    {
        public long Id { get; set; }
        public Guid PublicId { get; set; }
        public string Address { get; set; }
        public ICollection<EmailEmailAddress> EmailEmailAddresses { get; set; }
}
}
