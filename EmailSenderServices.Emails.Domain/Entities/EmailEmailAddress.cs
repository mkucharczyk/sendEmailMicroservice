﻿using System;

namespace EmailSender.Services.Emails.Domain.Entities
{
    public class EmailEmailAddress
    {
        public long EmailId { get; set; }
        public Email Email { get; set; }
        public long EmailAddressId { get; set; }
        public EmailAddress EmailAddress { get; set; }
    }
}
