﻿using EmailSender.Services.Emails.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace EmailSender.Services.Emails.Infrastructure
{
    public interface IEmailAddressRepository
    {
        public Task<EmailAddress> CreateOrGetByNameAsync(string address, CancellationToken cancellationToken);
        public Task<EmailEmailAddress> AddEmailEmailAddressPairAsync(Email email, EmailAddress emailAddress, CancellationToken cancellationToken);
    }
}