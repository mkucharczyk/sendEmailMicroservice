﻿using EmailSender.Services.Emails.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace EmailSender.Services.Emails.Domain.Repositories
{
    public interface IEmailRepository
    {
        Task<Email> GetAsync(Guid publicId, CancellationToken cancellationToken);
        Task<IEnumerable<Email>> GetAllAsync(CancellationToken cancellationToken);
        Task AddAsync(Email email, CancellationToken cancellationToken);
        Task<Guid> UpdateAsync(Guid emailId, string body = "", EmailAddress from = null, ICollection<EmailAddress> to = default, string subject = "", Status status = default, string sender = "", CancellationToken cancellationToken = default);
    }
}
