﻿using System;
using Xunit;
using EmailSender.Services.Emails.Application.Commands;
using EmailSender.Services.Emails.Application.UnitTests.Common;
using MediatR;
using Moq;
using System.Threading;
using EmailSender.Services.Emails.Infrastructure.SendGrid;

namespace Application.UnitTests
{
    public class SendEmailCommandTests : CommandTestBase
    {
        private readonly Mock<IMediator> mediatorMock;
        private readonly SendEmailCommandHandler handler;

        public SendEmailCommandTests()
        {
            mediatorMock = new Mock<IMediator>();
            var sendGridServiceMock = new Mock<ISendGridService>();
            handler = new SendEmailCommandHandler(mediatorMock.Object, EmailRepository, sendGridServiceMock.Object);
        }

        [Fact]
        public async void Given_HandleAddEmailCommand_Should_RaiseEmailCreatedNotification()
        {
            var emailId = Guid.NewGuid();

            var result = await handler.Handle(new SendEmailCommand(), CancellationToken.None);

           mediatorMock.Verify(m => m.Publish(It.Is<EmailSent>(e => e.EmailId == result), It.IsAny<CancellationToken>()), Times.Once);
        }
    }
}
