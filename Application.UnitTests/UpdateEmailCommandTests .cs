﻿using System;
using Xunit;
using EmailSender.Services.Emails.Application.Commands;
using EmailSender.Services.Emails.Application.UnitTests.Common;
using MediatR;
using Moq;
using System.Threading;

namespace Application.UnitTests
{
    public class UpdateEmailCommandTests : CommandTestBase
    {
        private readonly Mock<IMediator> mediatorMock;
        private readonly UpdateEmailCommandHandler handler;

        public UpdateEmailCommandTests()
        {
            mediatorMock = new Mock<IMediator>();
            handler = new UpdateEmailCommandHandler(mediatorMock.Object, EmailRepository);
        }

        [Fact]
        public async void Given_HandleUpdateEmailCommand_Should_RaiseEmailCreatedNotification()
        {
            var emailId = Guid.NewGuid();

            var result = await handler.Handle(new UpdateEmailCommand(), CancellationToken.None);

            mediatorMock.Verify(m => m.Publish(It.Is<EmailUpdated>(e => e.EmailId == result), It.IsAny<CancellationToken>()), Times.Once);
        }
    }
}
