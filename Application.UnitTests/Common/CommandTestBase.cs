﻿using EmailSender.Services.Emails.Infrastructure;
using System;

namespace EmailSender.Services.Emails.Application.UnitTests.Common
{
    public class CommandTestBase : IDisposable
    {
        protected readonly EmailContext Context;
        protected readonly EmailRepository EmailRepository;
        protected readonly EmailAddressRepository EmailAddressRepository;

        public CommandTestBase()
        {
            Context = EmailContextFactory.Create();
            EmailAddressRepository = new EmailAddressRepository(Context);
            EmailRepository = new EmailRepository(Context, EmailAddressRepository);
        }

        public void Dispose()
        {
            EmailContextFactory.Destroy(Context);
        }
    }
}
