﻿using EmailSender.Services.Emails.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;

namespace EmailSender.Services.Emails.Application.UnitTests.Common
{
    class EmailContextFactory
    {
        public static EmailContext Create()
        {
            var options = new DbContextOptionsBuilder<EmailContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            var context = new EmailContext(options);
            context.Database.EnsureCreated();
            context.SaveChanges();

            return context;
        }

        public static void Destroy(EmailContext context)
        {
            context.Database.EnsureDeleted();

            context.Dispose();
        }
    }
}
