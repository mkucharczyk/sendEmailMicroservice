﻿using System;
using Xunit;
using EmailSender.Services.Emails.Application.Commands;
using EmailSender.Services.Emails.Application.UnitTests.Common;
using MediatR;
using Moq;
using System.Threading;

namespace Application.UnitTests
{
    public class CreateEmailCommandTests : CommandTestBase
    {
        private readonly Mock<IMediator> mediatorMock;
        private readonly CreateEmailCommandHandler handler;

        public CreateEmailCommandTests()
        {
            mediatorMock = new Mock<IMediator>();
            handler = new CreateEmailCommandHandler(mediatorMock.Object, EmailRepository);
        }

        [Fact]
        public async void Given_HandleCreateEmailCommand_Should_RaiseEmailCreatedNotification()
        {
            var emailId = Guid.NewGuid();

            var result = await handler.Handle(new CreateEmailCommand(), CancellationToken.None);

            mediatorMock.Verify(m => m.Publish(It.Is<EmailCreated>(e => e.EmailId == result), It.IsAny<CancellationToken>()), Times.Once);
        }
    }
}
