﻿using SendGrid;
using SendGrid.Helpers.Mail;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace EmailSender.Services.Emails.Infrastructure.SendGrid
{
    public class SendGridService : ISendGridService
    {
        private readonly string apiKey;
        private readonly SendGridClient sendGridClient;
        private readonly IConfiguration configuration;

        public SendGridService(IConfiguration configuration)
        {
            apiKey = configuration["ApiKey"];
            sendGridClient = new SendGridClient(apiKey);
            this.configuration = configuration;
        }

        public async Task<Response> SendMessage(Email email)
        {
            EmailAddress fromAddress = null;
            if (email.From != null)
                fromAddress = MailHelper.StringToEmailAddress(email.From.Address);
            else
            {
                fromAddress = MailHelper.StringToEmailAddress(configuration["FromAddress"]);
            }
            fromAddress.Name = email.Sender;
            var msg = new SendGridMessage()
            {
                From = fromAddress,
                Subject = email.Subject,
                PlainTextContent = email.Body,
            };

            foreach (var address in email.To)
            {
                msg.AddTo(address.EmailAddress.Address);
            }
            return await sendGridClient.SendEmailAsync(msg);
        }
    }
}

