﻿using EmailSender.Services.Emails.Application;
using EmailSender.Services.Emails.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace EmailSender.Services.Emails.Infrastructure
{
    public class EmailAddressRepository : IEmailAddressRepository
    {
        private readonly IEmailContext emailContext;

        public EmailAddressRepository(IEmailContext emailContext)
        {
            this.emailContext = emailContext;
        }

        public async Task<EmailAddress> CreateOrGetByNameAsync(string address, CancellationToken cancellationToken = default)
        {
            var emailAddress = await emailContext.EmailAddresses.SingleOrDefaultAsync(e => e.Address == address, cancellationToken);

            if (emailAddress != null)
                return emailAddress;

            emailAddress = new EmailAddress()
            {
                PublicId = Guid.NewGuid(),
                Address = address,
            };
            await emailContext.EmailAddresses.AddAsync(emailAddress, cancellationToken);

            await emailContext.SaveChangesAsync(cancellationToken);
            return emailAddress;
        }

        public async Task<EmailEmailAddress> AddEmailEmailAddressPairAsync(Email email, EmailAddress emailAddress, CancellationToken cancellationToken = default)
        {
            var entity = await emailContext.EmailEmailAddresses.SingleOrDefaultAsync(x => x.Email.PublicId == email.PublicId && x.EmailAddress.PublicId == emailAddress.PublicId);
            if (entity != null)
                return entity;

            var emailEmailAddress = new EmailEmailAddress
            {
                Email = email,
                EmailAddress = emailAddress
            };
            await emailContext.EmailEmailAddresses.AddAsync(emailEmailAddress);

            await emailContext.SaveChangesAsync(cancellationToken);
            return emailEmailAddress;
        }
    }
}
