﻿using EmailSender.Services.Emails.Application;
using System.Threading.Tasks;

namespace EmailSender.Services.Emails.Infrastructure
{
    public class NotificationService : INotificationService
    {
        public Task SendAsync(MessageDto message)
        {
            return Task.CompletedTask;
        }
    }
}
