﻿// <auto-generated />
using System;
using EmailSender.Services.Emails.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace EmailSender.Services.Emails.Infrastructure.Persistance.Migrations
{
    [DbContext(typeof(EmailContext))]
    [Migration("20200907114737_InitialMigration")]
    partial class InitialMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.7")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("EmailSender.Email", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Body")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("DateSent")
                        .HasColumnType("datetime2");

                    b.Property<long?>("FromId")
                        .HasColumnType("bigint");

                    b.Property<string>("Priority")
                        .HasColumnType("nvarchar(max)");

                    b.Property<Guid>("PublicId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Sender")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("Status")
                        .HasColumnType("int");

                    b.Property<string>("Subject")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("FromId");

                    b.ToTable("Emails");
                });

            modelBuilder.Entity("EmailSender.Services.Emails.Domain.Entities.EmailAddress", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Address")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<Guid>("PublicId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.ToTable("EmailAddresses");
                });

            modelBuilder.Entity("EmailSender.Services.Emails.Domain.Entities.EmailEmailAddress", b =>
                {
                    b.Property<long>("EmailAddressId")
                        .HasColumnType("bigint");

                    b.Property<long>("EmailId")
                        .HasColumnType("bigint");

                    b.HasKey("EmailAddressId", "EmailId");

                    b.HasIndex("EmailId");

                    b.ToTable("EmailEmailAddresses");
                });

            modelBuilder.Entity("EmailSender.Email", b =>
                {
                    b.HasOne("EmailSender.Services.Emails.Domain.Entities.EmailAddress", "From")
                        .WithMany()
                        .HasForeignKey("FromId");
                });

            modelBuilder.Entity("EmailSender.Services.Emails.Domain.Entities.EmailEmailAddress", b =>
                {
                    b.HasOne("EmailSender.Email", "Email")
                        .WithMany("To")
                        .HasForeignKey("EmailAddressId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("EmailSender.Services.Emails.Domain.Entities.EmailAddress", "EmailAddress")
                        .WithMany("EmailEmailAddresses")
                        .HasForeignKey("EmailId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
