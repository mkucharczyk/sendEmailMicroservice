﻿using EmailSender.Services.Emails.Application;
using EmailSender.Services.Emails.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace EmailSender.Services.Emails.Infrastructure
{
    public class EmailContext : DbContext, IEmailContext
    {
        public EmailContext(DbContextOptions<EmailContext> options)
            : base(options)
        {
        }

        public DbSet<Email> Emails { get; set; }
        public DbSet<EmailAddress> EmailAddresses { get; set; }
        public DbSet<EmailEmailAddress> EmailEmailAddresses { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(EmailContext).Assembly);
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken) => await base.SaveChangesAsync();
    }
}
