﻿using EmailSender.Services.Emails.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EmailSender.Services.Emails.Infrastructure.Persistance
{
    public class EmailEmailAddressConfiguration : IEntityTypeConfiguration<EmailEmailAddress>
    {
        public void Configure(EntityTypeBuilder<EmailEmailAddress> builder)
        {
            builder.HasKey(eea => new
            {
                eea.EmailAddressId,
                eea.EmailId
            });

            builder.HasOne(ee => ee.Email)
                .WithMany(e => e.To)
                .HasForeignKey(e => e.EmailAddressId);
            builder.HasOne(eea => eea.EmailAddress)
                .WithMany(e => e.EmailEmailAddresses)
                .HasForeignKey(e => e.EmailId);
        }
    }
}
