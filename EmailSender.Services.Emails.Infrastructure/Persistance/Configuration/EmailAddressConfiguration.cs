﻿using EmailSender.Services.Emails.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EmailSender.Services.Emails.Infrastructure.Persistance
{
    public class EmailAddressConfiguration : IEntityTypeConfiguration<EmailAddress>
    {
        public void Configure(EntityTypeBuilder<EmailAddress> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Address).IsRequired();
        }
    }
}


