﻿using EmailSender.Services.Emails.Application;
using EmailSender.Services.Emails.Domain.Entities;
using EmailSender.Services.Emails.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace EmailSender.Services.Emails.Infrastructure
{
    public class EmailRepository : IEmailRepository
    {
        private readonly IEmailContext emailContext;
        private readonly IEmailAddressRepository emailAddressRepository;

        public EmailRepository(IEmailContext emailContext, IEmailAddressRepository emailAddressRepository)
        {
            this.emailContext = emailContext;
            this.emailAddressRepository = emailAddressRepository;
        }

        public async Task AddAsync(Email email, CancellationToken cancellationToken = default)
        {
            await emailContext.Emails.AddAsync(email, cancellationToken);

            await emailContext.SaveChangesAsync(cancellationToken);
        }

        public async Task<IEnumerable<Email>> GetAllAsync(CancellationToken cancellationToken = default)
        {
            return await emailContext.Emails
                .Include(e => e.To)
                .ToListAsync(cancellationToken);
        }

        public async Task<Email> GetAsync(Guid publicId, CancellationToken cancellationToken = default)
        {
            return await emailContext.Emails
                .Include(e => e.To)
                .SingleAsync(e => e.PublicId == publicId, cancellationToken);
        }

        public async Task<Guid> UpdateAsync(Guid emailId, string body = "", EmailAddress from = null, ICollection<EmailAddress> to = default, string subject = "", Status status = default, string sender = "", CancellationToken cancellationToken = default)
        {
            var entity = await emailContext.Emails.SingleAsync(e => e.PublicId == emailId, cancellationToken);

            if (!string.IsNullOrEmpty(body))
                entity.SetBody(body);
            if (!string.IsNullOrEmpty(sender))
                entity.SetSender(sender);
            if (!string.IsNullOrEmpty(subject))
                entity.SetSubject(subject);
            if (to.Count > 0)
            {
                ICollection<EmailEmailAddress> collection = new HashSet<EmailEmailAddress>();
                foreach (var address in to)
                {
                    var emailAddress = await emailAddressRepository.CreateOrGetByNameAsync(address.Address, cancellationToken);
                    var pair = await emailAddressRepository.AddEmailEmailAddressPairAsync(entity, emailAddress, cancellationToken);
                    collection.Add(pair);
                }

                entity.SetTo(collection);
            }
            if (status == Status.Sent)
                entity.SetSent();
            if (from != null)
            {
                var address = await emailAddressRepository.CreateOrGetByNameAsync(from.Address, cancellationToken);
                entity.SetFrom(address);
            }

            await emailContext.SaveChangesAsync(cancellationToken);
            return emailId;
        }
    }
}
