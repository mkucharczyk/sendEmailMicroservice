﻿using EmailSender.Services.Emails.Application;
using EmailSender.Services.Emails.Domain.Repositories;
using EmailSender.Services.Emails.Infrastructure.SendGrid;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace EmailSender.Services.Emails.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            //services.AddDbContext<EmailContext>(options =>
            //    options.UseSqlServer(configuration.GetConnectionString("EmailDatabase")));

            services.AddDbContext<EmailContext>(options => options.UseInMemoryDatabase(databaseName: "EmailSender")); ;

            services.AddScoped<IEmailContext>(provider => provider.GetService<EmailContext>());
            services.AddScoped<IEmailRepository, EmailRepository>();
            services.AddScoped<IEmailAddressRepository, EmailAddressRepository>();
            services.AddScoped<ISendGridService, SendGridService>();
            services.AddScoped<INotificationService, NotificationService>();
            return services;
        }
    }
}
