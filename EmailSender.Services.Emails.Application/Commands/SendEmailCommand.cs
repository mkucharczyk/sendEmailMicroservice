﻿using MediatR;
using System;

namespace EmailSender.Services.Emails.Application.Commands
{
    public class SendEmailCommand : IRequest<Guid>
    {
        public Guid EmailId { get; set; }
    }
}
