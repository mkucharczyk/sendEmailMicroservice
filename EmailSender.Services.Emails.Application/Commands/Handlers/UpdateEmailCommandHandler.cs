﻿using EmailSender.Services.Emails.Application.Exceptions;
using EmailSender.Services.Emails.Domain.Entities;
using EmailSender.Services.Emails.Domain.Repositories;
using EmailSender.Services.Emails.Infrastructure;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;

namespace EmailSender.Services.Emails.Application.Commands
{
    public class UpdateEmailCommandHandler : IRequestHandler<UpdateEmailCommand, Guid>
    {
        private readonly IMediator mediator;
        private readonly IEmailRepository emailRepository;

        public UpdateEmailCommandHandler(IMediator mediator, IEmailRepository emailRepository)
        {
            this.mediator = mediator;
            this.emailRepository = emailRepository;
        }

        public async Task<Guid> Handle(UpdateEmailCommand request, CancellationToken cancellationToken)
        {
            var email = await emailRepository.GetAsync(request.EmailId, cancellationToken);
            if(email.Status == Status.Sent)
            {
                throw new CannotModifySentEmailsException();
            }

            await emailRepository.UpdateAsync(request.EmailId, request.Body, request.From, request.To, request.Subject, request.Status, request.Sender, cancellationToken);

            await mediator.Publish(new EmailUpdated { EmailId = email.PublicId }, cancellationToken);

            return email.PublicId;

        }
    }
}
