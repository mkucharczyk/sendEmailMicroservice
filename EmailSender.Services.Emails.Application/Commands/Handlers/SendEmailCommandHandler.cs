﻿using EmailSender.Services.Emails.Domain.Repositories;
using EmailSender.Services.Emails.Infrastructure.SendGrid;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace EmailSender.Services.Emails.Application.Commands
{
    public class SendEmailCommandHandler : IRequestHandler<SendEmailCommand, Guid>
    {
        private readonly IMediator mediator;
        private readonly IEmailRepository emailRepository;
        private readonly ISendGridService sendGridService;

        public SendEmailCommandHandler(IMediator mediator, IEmailRepository emailRepository, ISendGridService sendGridService)
        {
            this.mediator = mediator;
            this.emailRepository = emailRepository;
            this.sendGridService = sendGridService;
        }

        public async Task<Guid> Handle(SendEmailCommand request, CancellationToken cancellationToken)
        {
            var email = await emailRepository.GetAsync(request.EmailId, cancellationToken);

            await sendGridService.SendMessage(email);
            await emailRepository.UpdateAsync(emailId: email.PublicId, status: Domain.Entities.Status.Sent, cancellationToken: cancellationToken);
            await mediator.Publish(new EmailSent { EmailId = email.PublicId }, cancellationToken);

            return email.PublicId;
        }
    }
}
