﻿using EmailSender.Services.Emails.Domain.Repositories;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace EmailSender.Services.Emails.Application.Commands
{
    public class CreateEmailCommandHandler : IRequestHandler<CreateEmailCommand, Guid>
    {
        private readonly IMediator mediator;
        private readonly IEmailRepository emailRepository;

        public CreateEmailCommandHandler(IMediator mediator, IEmailRepository emailContext)
        {
            this.mediator = mediator;
            this.emailRepository = emailContext;
        }

        public async Task<Guid> Handle(CreateEmailCommand request, CancellationToken cancellationToken)
        {
            var entity = new Email();

            await emailRepository.AddAsync(entity, cancellationToken);
            await mediator.Publish(new EmailCreated { EmailId = entity.PublicId }, cancellationToken);

            return entity.PublicId;
        }
    }
}
