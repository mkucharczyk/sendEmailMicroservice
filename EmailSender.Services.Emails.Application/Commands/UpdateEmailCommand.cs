﻿using EmailSender.Services.Emails.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;

namespace EmailSender.Services.Emails.Application.Commands
{
    public class UpdateEmailCommand : IRequest<Guid>
    {
        public Guid EmailId { get; set; }
        public EmailAddress From { get; set; }
        public ICollection<EmailAddress> To { get; set; }
        public string Sender { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public Status Status { get; set; }
    }
}
