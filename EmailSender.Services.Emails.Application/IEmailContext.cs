﻿using EmailSender.Services.Emails.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace EmailSender.Services.Emails.Application
{
    public interface IEmailContext
    {
        public DbSet<Email> Emails { get; set; }
        public DbSet<EmailAddress> EmailAddresses { get; set; }
        public DbSet<EmailEmailAddress> EmailEmailAddresses { get; set; }
        public Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
