﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace EmailSender.Services.Emails.Application.Commands
{
    public class EmailSent : INotification
    {
            public Guid EmailId { get; set; }

            public class EmailCreatedHandler : INotificationHandler<EmailSent>
            {
                private readonly INotificationService _notification;

                public EmailCreatedHandler(INotificationService notification)
                {
                    _notification = notification;
                }

                public async Task Handle(EmailSent notification, CancellationToken cancellationToken)
                {
                    await _notification.SendAsync(new MessageDto());
                }
        }
    }
}
