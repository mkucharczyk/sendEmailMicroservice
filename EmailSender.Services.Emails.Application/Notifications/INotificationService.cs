﻿using System.Threading.Tasks;

namespace EmailSender.Services.Emails.Application
{
    public interface INotificationService
    {
        Task SendAsync(MessageDto message);
    }
}
