﻿using System;

namespace EmailSender.Services.Emails.Application.Exceptions
{
    public class CannotModifySentEmailsException : Exception
    {
    }
}
