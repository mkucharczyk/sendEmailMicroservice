﻿using EmailSender.Services.Emails.Domain.Repositories;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace EmailSender.Services.Emails.Application.Queries.Handlers
{
    public class GetEmailQueryHandler : IRequestHandler<GetEmailQuery, Email>
    {
        private readonly IEmailRepository emailRepository;

        public GetEmailQueryHandler(IEmailRepository emailRepository)
        {
            this.emailRepository = emailRepository;
        }

        public async Task<Email> Handle(GetEmailQuery request, CancellationToken cancellationToken = default)
        {
            return await emailRepository.GetAsync(request.PublicId, cancellationToken);
        }
    }
}
