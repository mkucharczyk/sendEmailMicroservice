﻿using EmailSender.Services.Emails.Domain.Repositories;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace EmailSender.Services.Emails.Application.Queries.Handlers
{
    public class GetAllEmailsQueryHandler : IRequestHandler<GetAllEmailsQuery, GetAllEmailsVm>
    {
        private readonly IEmailRepository emailRepository;

        public GetAllEmailsQueryHandler(IEmailRepository emailRepository)
        {
            this.emailRepository = emailRepository;
        }

        public async Task<GetAllEmailsVm> Handle(GetAllEmailsQuery request, CancellationToken cancellationToken = default)
        {
            var emails = await emailRepository.GetAllAsync(cancellationToken);
            return new GetAllEmailsVm { Emails = emails };
        }
    }
}
