﻿using MediatR;
using System;

namespace EmailSender.Services.Emails.Application.Queries
{
    public class GetEmailQuery : IRequest<Email>
    {
        public Guid PublicId { get; set; }
    }
}
