﻿using MediatR;

namespace EmailSender.Services.Emails.Application.Queries
{
    public class GetAllEmailsQuery : IRequest<GetAllEmailsVm>
    {
    }
}
