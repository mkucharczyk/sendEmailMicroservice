﻿using System.Collections.Generic;

namespace EmailSender.Services.Emails.Application.Queries
{
    public class GetAllEmailsVm
    {
        public IEnumerable<Email> Emails { get; set; }
    }
}
