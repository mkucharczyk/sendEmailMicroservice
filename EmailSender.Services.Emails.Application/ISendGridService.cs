﻿using SendGrid;
using System.Threading.Tasks;

namespace EmailSender.Services.Emails.Infrastructure.SendGrid
{
    public interface ISendGridService
    {
        Task<Response> SendMessage(Email email);
    }
}