﻿using EmailSender.Services.Emails.Application.Commands;
using EmailSender.Services.Emails.Application.Queries;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace EmailSender.Services.Emails.Api.Controllers
{
    public class EmailsController : BaseController
    {
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Create([FromBody] CreateEmailCommand command)
        {
            await Mediator.Send(command);

            return NoContent();
        }

        [HttpGet("{publicId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<Email>> Get(Guid publicId)
        {
            return Ok(await Mediator.Send(new GetEmailQuery { PublicId = publicId }));
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<Email>> GetAll()
        {
            return Ok(await Mediator.Send(new GetAllEmailsQuery()));
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Send([FromBody] SendEmailCommand command)
        {
            await Mediator.Send(command);

            return NoContent();
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Update(UpdateEmailCommand command)
        {
            await Mediator.Send(command);

            return NoContent();
        }
    }
}
